my_ansible(){
  ansible_install
  sudo chown -R $USER:$USER /etc/ansible
  rm /etc/ansible/hosts
  ln -srf ~/.uni/my/hosts /etc/ansible
  ansible_history
}

my_git(){
  cp set/.gitconfig ~/
  git_history
}

ansible_history(){
 echo '''ansible -m ping cloud
ansible -m ping local
ansible-playbook uni''' \
  > ~/.bash_history
}

ansible_install(){
  if command -v apt > /dev/null;then
    sudo apt install -y ansible
  elif command -v dnf > /dev/null;then
    sudo dnf install -y ansible
  elif command -v pacman > /dev/null;then
    sudo pacman -Syu ansible
  fi
}

git_history(){
  echo '''git init
git add .
git commit -am 'a'
git tag v0.0
git push origin v0.0
git push
git remote get-url origin''' \
  >> ~/.bash_history
}

my_ansible
my_git
